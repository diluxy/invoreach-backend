const company = require("./company");

module.exports=(sequelize, DataTypes)=>{

const user =sequelize.define("user",{

     userId: {
        type: DataTypes.INTEGER,
        unique:true,
        allowNull:false,
        primaryKey: true,
        autoIncrement: true,
 },
     companyId:{
        type: DataTypes.STRING(6),
         allowNull: false,
        isUppercase: true, 
        references: {
            model: 'company',
            key: 'companyId'
          }
     },
    fullName:{
        type: DataTypes.STRING(20),
        allowNull: false,
    },
    emailAddress:{
        type: DataTypes.STRING(30),
        allowNull: false,
        unique: true,
        isEmail:true,
    }, 
        
    telephoneNumber:{
        type: DataTypes.STRING(15),
        allowNull: false,
        unique: true,
    },
    password:{
        type: DataTypes.STRING,
        allowNull: false,
    },
 },
     {
    freezeTableName: true
});
 
 return user;
}
