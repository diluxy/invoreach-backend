module.exports=(sequelize, DataTypes)=>{

    const company =sequelize.define("company",{
    
         companyId:{
              type: DataTypes.STRING(6),
              allowNull: false,
              primaryKey: true,
              unique:true,
              isUppercase: true,  
          }, 
        companyName:{
            type: DataTypes.STRING(30),
            allowNull: false,
        },
 },
        {
            freezeTableName: true
         });

 return company;
}