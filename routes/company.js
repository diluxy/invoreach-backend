module.exports =(app)=> {
    const company = require("../controllers/company");
  
    var router = require("express").Router();
  
    
    router.post("/", company.create);

    app.use('/api/company', router);
}; 