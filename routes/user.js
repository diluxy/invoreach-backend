module.exports =(app)=> {
    const user = require("../controllers/user");
  
    var router = require("express").Router();
  
    // Create a new user
router.post("/register",user.register)
router.post("/login",user.login)

    app.use('/api/user', router);
    

}; 