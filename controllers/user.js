const db= require('../models');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const user = require('../models/user');
const { Op } = require("sequelize");

function register(req, res){
    
    if (!req.body.companyId) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
    db.user.findOne({where:{emailAddress:req.body.emailAddress}}).then(result => {
      if(result){
          res.status(409).json({
              message: "Email already exists!",
          });
      }else{
          bcryptjs.genSalt(10, function(err, salt){
              bcryptjs.hash(req.body.password, salt, function(err, hash){
                const User = {
                  companyId: req.body.companyId,
                  fullName: req.body.fullName,
                  emailAddress:req.body.emailAddress,
                  telephoneNumber:req.body.telephoneNumber,
                  password:hash,
              }
                  db.user.create(User).then(result => {
                    res.status(201).json({
                        message: "User created successfully",
                    });
                }).catch(error => {
                  console.log(error)
                   res.status(500).json({
                        message: "Something went wrong!",
                    });
                });
            });
        });
    }
}).catch(error => {
    res.status(500).json({
        message: "Something went wrong!",
    });
  });
}

function login(req, res){
  db.user.findOne({where:{fullName: req.body.fullName}}).then(User => {
      if(User === null){
          res.status(401).json({
              message: "Invalid UserName ",
          });
      }else{
          bcryptjs.compare(req.body.password, User.password, function(err, result){
              if(result){
                  const token = jwt.sign({
                    companyId:User.companyId,
                     fullName:User.fullName,
                   
                  }, process.env.JWT_KEY, function(err, token){
                    console.log(err);
                      res.status(200).json({
                          message: "Authentication successful!",
                          token: token
                      });
                  });
                }else{
                res.status(401).json({
                  message: "Incorrect password!!",
              });
          }
      });
  }
}).catch(error => {
  console.log(error);
  res.status(500).json({
      message: "Something went wrong!",
  });
});
}

module.exports={
  register:register,
  login:login
}



  

  
