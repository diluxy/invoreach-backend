const db= require('../models');
const Company = db.company;


exports.create = (req, res) => {
    // Validate request
    if (!req.body.companyId) {
      res.status(400).send({
        message: "Content can not be empty!!"
      });
      return;
    }
  
    // Create a Tutorial
    const company = {
      companyId: req.body.companyId,
      companyName: req.body.companyName,
     
    };
    
  // Save Tutorial in the database
  Company.create(company)
  .then(data => {
    res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message:
        err.message || "Some error occurred while creating the user."
    });
  });
};